package com.ymt.datajpa.dao;

import com.ymt.datajpa.models.Cliente;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ClienteDaoImpl implements IClienteDao {

    @PersistenceContext
    private EntityManager em;

    @SuppressWarnings("unchecked")
    @Override
    public List<Cliente> findAll() {
        // TODO Auto-generated method stub
        return em.createQuery("from Cliente").getResultList();
    }

    @Override
    public Cliente findOne(Long id) {
        return em.find(Cliente.class, id);
    }

    @Override
    public void save(Cliente cliente) {
        if(cliente.getId() != null && cliente.getId() >0) {
            em.merge(cliente);//editar
        } else {
            em.persist(cliente);//inserta
        }
    }


    @Override
    public void delete(Long id) {
        em.remove(findOne(id));
    }

}